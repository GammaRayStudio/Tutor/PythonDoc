Python Flask , 快速開始
======
`輕量級網頁框架`

課程大綱
------
+ 快速開始
  + 安裝模組
  + 五行程式碼
  + Mirco 的定義
  + Flask vs Django
  + 開發配置
+ 註冊路由
  + 參數傳遞
  + Http 方法
  + URL 轉址
+ 網頁模板
  + Jinja2 模板引擎
  + 網頁模板
  + 靜態資源
+ 資料交換
  + Html : Form 表單提交
  + jQuery : Ajax 非同步
  + RESTful 風格 API
+ Cookies 使用
+ Sessions 使用
+ Message Flashing 功能
+ File Uploading 實作
+ Logging 功能
+ 資料庫整合 `SQL`
  + 使用 SQLite
  + 連線 MySQL
  + ORM 框架 : SQL Alchemy
  + 整合模組 : Flask_SQLAlchemy
+ 範例實作 : 任務清單
  + 查詢任務
  + 新增任務
  + 修改任務 
  + 刪除任務
+ 範例實作 : 會員功能
  + 註冊會員
  + 登入會員
  + 更新資料
  + 停止會員
+ 雲端部署
  + Heroku 
  + GCP (Google Cloud Platform)


參考資料
------
### Flask 官網
<https://flask.palletsprojects.com/en/2.0.x/>

### Tutorialspoint - Flask
<https://www.tutorialspoint.com/flask/index.htm>



