Python Flask , 註冊路由
======
`Router`

### 五行程式碼
```py
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, World!"
```

### Hello World 範例

**WebSE.py**

```py
from flask import Flask
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World'

if __name__ == '__main__':
    app.run()
```

+ 加上 python 的 main 方法，執行 app.run() 函式 
  + `執行網頁伺服器的啟動動作`
+ `@app.router('/hello')` : 導向網站 `/hello` 的 URL 路徑

<br>


參數傳遞
------
`訪問 URL 的位址，作為函式接收的參數`

**001-1.routing-path-param.py**

```py
from flask import Flask
app = Flask(__name__)

@app.route('/api/<name>')
def apiPathString(name):
    print("type(name) : ", type(name))
    return 'String => {}'.format(name)

@app.route('/api/<int:row_id>')
def apiPathInteger(row_id):
    print("type(id) : ", type(row_id))
    return 'int => {}'.format(row_id)

@app.route('/api/<float:version>')
def apiPathFloat(version):
    print("type(version) : ", type(version))
    return 'float => {}'.format(version)

if __name__ == '__main__':
    app.run()
```

<br>

### 接收字串型態
```py
@app.route('/api/<name>')
def apiPathString(name):
    print("type(name) : ", type(name))
    return 'String => {}'.format(name)
```

`<name>` 代表接收 name 參數為 字串型態

+ 訪問 : `http://127.0.0.1:5000/api/FlaskSE`
+ 打印 : `type(name) :  <class 'str'>`
+ 網頁 : `String => FlaskSE`

<br>

### 接收整數型態
```py
@app.route('/api/<int:row_id>')
def apiPathInteger(row_id):
    print("type(id) : ", type(row_id))
    return 'int => {}'.format(row_id)
```

+ 訪問 : `http://127.0.0.1:5000/api/5`
+ 打印 : `type(row_id) :  <class 'int'>`
+ 網頁 : `int => 5`

<br>

### 接收浮點數型態
```py
@app.route('/api/<float:version>')
def apiPathFloat(version):
    print("type(version) : ", type(version))
    return 'float => {}'.format(version)
```

+ 訪問 : `http://127.0.0.1:5000/api/1.01`
+ 打印 : `type(version) :  <class 'float'>`
+ 網頁 : `float => 1.01`

<br>

Http 方法
------
**001-2.routing-http-method.py**

```py
from flask import Flask, request

app = Flask(__name__)

@app.route('/api', methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
def http_method():
    if request.method == "GET":
        return 'Http Method => GET'
    elif request.method == "POST":
        return 'Http Method => POST'
    elif request.method == "PUT":
        return 'Http Method => PUT'
    elif request.method == "PATCH":
        return 'Http Method => PATCH'
    elif request.method == "DELETE":
        return 'Http Method => DELETE'


@app.route('/api/get')
def http_get():
    if request.method == "GET":
        return 'Http Method => GET'
    else:
        return 'Http Method => Other'

if __name__ == '__main__':
    app.run()
```

+ 導入 flask 中的 request 模組
+ 註冊的路由可配置 http 的 methods 參數
  + 未設置 methods 參數，預設為 GET 方法
  + 網頁訪問 : `http://127.0.0.1:5000/api/get`
  + CURL 測試 : `curl -X POST http://127.0.0.1:5000/api/get` (顯示不允許訪問的訊息)
+ HTTP 其他方法測試 : CURL 測試
  + `curl -X GET http://127.0.0.1:5000/api` 
  + `curl -X POST http://127.0.0.1:5000/api` 
  + `curl -X PATCH http://127.0.0.1:5000/api` 
  + `curl -X PUT http://127.0.0.1:5000/api` 
  + `curl -X DELETE http://127.0.0.1:5000/api` 

**CURL for Windows**
+ <https://curl.se/windows/>

<br>

URL 轉址
------
**001-3.routing-redirect-url.py**

```py
from flask import Flask, request, redirect, url_for
app = Flask(__name__)

@app.route('/navgation/<target>', methods=['GET'])
def navgation(target):
    if request.method == "GET":
        if target == "A":
            return redirect(url_for('moduleA'))
        elif target == "B":
            return redirect(url_for('module', module_type='B'))
        else:
            return redirect(url_for('system_message', msg_type='error'))

@app.route('/module/A', methods=['GET'])
def moduleA():
    if request.method == "GET":
        return "Module A"

@app.route('/module/<module_type>', methods=['GET'])
def module(module_type):
    if request.method == "GET":
        return "Module {}".format(module_type)

@app.route('/system/message/<msg_type>', methods=['GET'])
def system_message(msg_type):
    if request.method == "GET":
        return "System {} => {}".format(msg_type, "Could not found this module.")

if __name__ == '__main__':
    app.run()
```

+ 導入 flask 中的 redirect 與 url_for 模組
  + 格式 : redirec(url_for(`{路由函式}`, `{路徑參數}` = '`{參數數值}`' , `more parameter ... etc`))
+ `navigation` 路徑 : 依據 `target` 位置跳轉目標模組
  + `module A` 函式 : 跳轉 module A 功能頁面
  + `module` 函式 : 跳轉 module 功能頁面，並且傳入模組型態的參數
  + `system_messag` 函式 : 非規格內的模組功能，跳轉到系統頁面，提示例外訊息



