VSCode - Python IDE
======
`Visual Studio Code - Python Integrated Development Environment`

+ 官網 : <https://code.visualstudio.com/>
+ 下載 : <https://code.visualstudio.com/download>

<br>

### Plugin
+ Python `Mircosoft`

<br>

### KeyMap 
+ Mac : <https://code.visualstudio.com/shortcuts/keyboard-shortcuts-macos.pdf>
+ Win : <https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf>

**Mac**
+ Explorer : cmd + shift + E
+ Terminal : ctrl + `
+ Format : opt + shift + F

**Win**
+ Explorer : ctrl + shift + E
+ Terminal : ctrl + `
+ Format : alt + shift + F

<br>

### Custom
`cmd + shift + P / ctrl + shift + P`
+ Python: Run Python File in Terminal : opt + R



