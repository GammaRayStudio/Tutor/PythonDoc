Python , SQLite3 模組
======
`已預設於標準程式庫內`

安裝軟體
------
### DB Browser for SQLite
<https://sqlitebrowser.org/>

### DBeaver
<https://dbeaver.io/>


創建檔案
------
`副檔名 : .db 或 .sqlite`

### 拖曳到 DB Browser for SQLite
![001](assets/001/001.sqlite-file.jpeg)


新增表格
------
`右上角 Create Table 創建表格`

![002](assets/001/002.create-table.jpeg)

+ id : Integer , Not Null , Primary Key `識別碼`
+ name : Text `應用程式名稱`
+ version : Text `發佈版本`
+ author : Text `開發者`
+ date : Text `發佈日期`
+ remark : Text `備註描述`

**SQL - DDL**
```sql
CREATE TABLE "app_info" (
	"id"	INTEGER NOT NULL,
	"name"	TEXT,
	"version"	TEXT,
	"author"	TEXT,
	"date"	TEXT,
	"remark"	TEXT,
	PRIMARY KEY("id")
);
```

新增資料
------
`切換 Browse Data 頁籤`

![003](assets/001/003.add-data.jpeg)

1. 點擊新增資料的按鈕 `表格與加號的圖示`
2. 由於配置 PK 鍵，id 欄位自動配置流水號
3. 如同其他的編輯檔案的軟體一樣，修改後要儲存才會生效


執行 SQL
------
`切換「執行SQL」頁籤`

![004](assets/001/004.exec-sql.jpeg)

**查詢**

```sql
SELECT * FROM app_info WHERE id = 1;
```

**新增**
```sql
INSERT INTO app_info (name, version, author, date, remark)
VALUES ('App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1');
```

**修改**
```sql
UPDATE app_info 
SET name='AppNew', version='1.0.2', remark='AppNew-v1.0.2'
WHERE id=1;
```

**刪除**
```sql
DELETE FROM app_info WHERE id=2;
```

### 使用 DBeaver

**建立連線**
![005-1](assets/001/005-1.dbeaver-conn.png)

**檔案路徑**
![005-2](assets/001/005-2.dbeaver-path.png)

**執行 SQL**
![005-3](assets/001/005-3.dbeaver-sql.png)



使用 Python 連線 SQLite
------
`使用 Python 標準庫內的 sqlite3 模組`

<br>

**001-1.sqlite-conn.py**

`SQLite 資料庫連線 與 執行查詢的SQL 語法`

```py
import sqlite3 #導入模組

conn = sqlite3.connect('DevDb.db') #建立連線

cursor = conn.cursor() #取得游標物件
cursor.execute('SELECT * FROM `app_info`;') #執行 SQL 語法

records = cursor.fetchall() #取得回傳資料
print(records)
print("record type => ", type(records)) #回傳資料型態
print("record[0] type => ", type(records[0])) #回傳資料內部的元素型態

for r in records: # list 型態，可回圈迭代
    print(r)

print("\n---\n")

for r in records:
    app_id, app_name, app_ver, app_author, app_date, app_remark = r # tuple 元素拆解
    print("id => ", app_id)
    print("name => ", app_name)
    print("\n---\n")

cursor.close() # 釋放資源
conn.close()
```

Output:

```
[(1, 'App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1')]
record type =>  <class 'list'>
record[0] type =>  <class 'tuple'>
(1, 'App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1')

---

id =>  1
name =>  App

---
```


SQLite : 新增、修改、刪除
------
**001-2.sqlite-ctrl.py**

`用 While 迴圈與 input 方法，實作依據動作代碼，執行相應操作的互動程式`

```py
import sqlite3

conn = sqlite3.connect('DevDb.db')

cursor = conn.cursor()

appDesc = """
Please input action code :

1 - Insert Data
2 - Update Data
3 - Delete Date
--- --- ---
0 - exit

"""

isRun = True
while isRun:
    cursor.execute('SELECT * FROM `app_info`;')
    records = cursor.fetchall()

    for r in records:
        print(r)

    ctrl = input(appDesc)
    if ctrl == "0": # 結束程式
        isRun = False
    elif ctrl == "1": # 執行插入的 SQL 語法
        sql = """
        INSERT INTO app_info (name, version, author, date, remark) 
        VALUES ('App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1');
        """
        cursor.execute(sql)
        conn.commit() # 新增的語法必須要再加上提交的操作
    elif ctrl == "2": # 執行更新的 SQL 語法
        row_id = input("row_id = ? ") # input 方法，動態決定目標 id
        sql = """
        update app_info
        set name = 'AppNew' , version='1.0.2' , remark = 'App-v1.0.2' 
        WHERE id={};
        """.format(row_id)
        cursor.execute(sql)
        conn.commit()# 更新的語法必須要再加上提交的操作
    elif ctrl == "3": # 執行刪除的 SQL 語法
        row_id = input("row_id = ? ") # input 方法，動態決定目標 id
        sql = """
        delete from app_info
        where id={};
        """.format(row_id)
        cursor.execute(sql)
        conn.commit()# 刪除的語法必須要再加上提交的操作

cursor.close()
conn.close()
```

<br>

新增操作`(動作代碼1)`:
```
(1, 'PythonSQL', '1.0.1', 'DevAuth', '2021-11-20', 'PythonSQL-v1.0.1')

Please input action code :

1 - Insert Data
2 - Update Data
3 - Delete Date
--- --- ---
0 - exit

1
(1, 'PythonSQL', '1.0.1', 'DevAuth', '2021-11-20', 'PythonSQL-v1.0.1')
(2, 'App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1')

Please input action code :

1 - Insert Data
2 - Update Data
3 - Delete Date
--- --- ---
0 - exit

1
(1, 'PythonSQL', '1.0.1', 'DevAuth', '2021-11-20', 'PythonSQL-v1.0.1')
(2, 'App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1')
(3, 'App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1')
```

<br>

修改操作`(動作代碼2)`:
```
Please input action code :

1 - Insert Data
2 - Update Data
3 - Delete Date
--- --- ---
0 - exit

2
row_id = ? 2
(1, 'PythonSQL', '1.0.1', 'DevAuth', '2021-11-20', 'PythonSQL-v1.0.1')
(2, 'AppNew', 'v1.0.2', 'DevAuth', '2021-11-20', 'App-v1.0.2')
(3, 'App', 'v1.0.1', 'DevAuth', '2021-11-20', 'App-v1.0.1')
```

<br>

刪除操作`(動作代碼3)`:
```
Please input action code :

1 - Insert Data
2 - Update Data
3 - Delete Date
--- --- ---
0 - exit

3
row_id = ? 3
(1, 'PythonSQL', '1.0.1', 'DevAuth', '2021-11-20', 'PythonSQL-v1.0.1')
(2, 'AppNew', 'v1.0.2', 'DevAuth', '2021-11-20', 'App-v1.0.2')
```

<br>

SQLite 應用場景
------
`數據量小、單機程式`

+ 小型網站的資料庫
+ 單機應用程式的資料保存
+ 行動應用程式的資料保存


