控制流程
======


Contents
------
+ if . . else 條件式
+ while 迴圈
+ for 迴圈
+ pass、break、continue
+ for Comprehension 語法


基本流程語法
------

```
在 Python 中，一個程式區塊是使用冒號「 : 」開頭，
之後同一區塊範圍要有相同的縮排，不可混用不同空白數量，不可混用空白與 Tab，
Python 的建議是使用四個空白作為縮排。
```

if .. else 條件式
------
`001.ifElse.py`

### if .. else
```py
if 條件式 :
    // 陳述句
else :
    if 條件式 :
        // 陳述句
    else:
        // 陳述句
```


### if .. elif .. else
```py
if 條件式 :
    // 陳述句
elif 條件式 :
    // 陳述句
elif 條件式 :
    // 陳述句
elif 條件式 :
    // 陳述句
else : 
    // 陳述句
```

java
```java
if(條件式){
    // 陳述句
}else if(條件式){
    // 陳述句
}else{
    // 陳述句
}
```

**Source Code**
```py
num = 15

if num < 5:
    print(" num < 5")
elif 5 < num < 10:
    print(" 5 < num < 10")
else:
    print("num > 10")
```

Output :
```
num > 10
```

<br>

### Python 的 if..else 運算式

```py
num01 = 10
num02 = 15
max = num01 if num01 > num02 else num02
print("max => ", max)
```

Output :
```
max => 15
```

    當 if 的條件時成立時，會傳回 if 當前的數值，若不成立則傳回 else 後的數值。

<br>

**為什麼Python中沒有switch或case語句？**
`Python FAQ`
```
你可以通過一系列 if... elif... elif... else.輕松完成這項工作。
對於switch語句語法已經有了一些建議，但尚未就是否以及如何進行范圍測試達成共識。
有關完整的詳細信息和當前狀態，請參閱 PEP 275 。
```
對於需要從大量可能性中進行選擇的情況，可以創建一個字典，

將case 值映射到要調用的函數。例如：

```py
def function_1(...):
    ...

functions = {'a': function_1,
             'b': function_2,
             'c': self.method_1, ...}

func = functions[value]
func()
```
對於對象調用方法，可以通過使用 getattr() 內置檢索具有特定名稱的方法來進一步簡化：
```py
def visit_a(self, ...):
    ...
...

def dispatch(self, value):
    method_name = 'visit_' + str(value)
    method = getattr(self, method_name)
    method()
```
建議對方法名使用前綴，例如本例中的 visit_ 。

如果沒有這樣的前綴，如果值來自不受信任的來源，攻擊者將能夠調用對象上的任何方法。

+ [Python FAQ]

<br>


while 迴圈
------
`002.white.py`
```py
while 條件式 :
    // 陳述式
else : 
    // 陳述式
```

+ else 是其他程式語言幾乎沒有的特色
+ 書籍不建議使用，不太容易理解語法，參見範例二

### white
```py
count = 1
while count < 10
    print("count => ", count)
    count += 1
print("loop => end")
```

Output :
```
count =>  0
count =>  1
count =>  2
count =>  3
count =>  4
count =>  5
count =>  6
count =>  7
count =>  8
count =>  9
loop => end
```

<br>

### white .. else
```py
print("\nwheil .. else:")
count = 1
while count < 5:
    print("count => ", count)
    count += 1
else:
    print("else => end")
```

Output :
```
wheil .. else:
count =>  1
count =>  2
count =>  3
count =>  4
else => end
```

+ 容易被誤認為相似 if..else 語法，若沒執行 while 迴圈就執行 else 部分
+ 但 while 迴圈正常執行結束後，也會執行 else 部分。

<br>

### white .. break .. else
```py
print("\nwheil .. break .. else :")
count = 1
while count < 5:
    print("count => ", count)
    count += 1
    if count == 3:
        break
else:
    print("else => else")
```

Output :
```
wheil .. break .. else :
count =>  1
count =>  2
```

+ if 條件成立，執行 break，然後就不會執行 else `<- 不容易理解`
+ if 沒有執行 break，就會執行 else `<- 比較相近的意思`
+ 兩者都不太容易理解，書籍建議別使用 while 與 else 形式

<br>


for in 迭代
------
`003.forIn.py`

**迭代序列**
+ 字串
+ list
+ tuple

```
for 變數 in 序列物件 :
    // 陳述式
```

### for in
```py
lstData = ['001', '002', '003', '004', '005']
for text in lstData:
    print(text)

lstDict = {'001': 'A', '002': 'B', '003': 'C'}
for key, val in lstDict.items():
    print('key = ', key, ' , val = ', val)
for key in lstDict.keys():
    print('key = ', key)
for val in lstDict.values():
    print('val = ', val)
```

Output :
```
001
002
003
004
005
key =  001  , val =  A
key =  002  , val =  B
key =  003  , val =  C
key =  001
key =  002
key =  003
val =  A
val =  B
val =  C
```

+ 迭代 dict 鍵值
  + items() => dict_keys 物件
  + keys() => dict_values 物件
  + values() => dict_items 物件

### for (i;i<n;i++)
```
for i in range(len(lstData)):
    print("{} => {}".format(i, lstData[i]))
```

Output :
```
0 => 001
1 => 002
2 => 003
3 => 004
4 => 005
```

+ range() 函式 : 產生指定的數字範圍
+ len() 函式 : 取得串列的長度


java
```java
for(初始式; 判斷式 ; 重複式){
    // 陳述式
}
```

### for in .. else
```py
lstData = ['001', '002', '003', '004', '005']
for text in lstData:
    print(text)
else:
    print("else => end")
```

Output :
```
001
002
003
004
005
else => end
```

### for in .. break .. else
```py
lstData = ['001', '002', '003', '004', '005']
for text in lstData:
    print(text)
    if text == '003':
        break
else:
    print("else => end")
```

Output :
```
001
002
003
```

### dir() => `__iter__()`

    只要是實作了__iter__()方法的物件，都可以使用for in來迭代

```bash
python -c "print(dir(list))"
```

Message :
```
['__add__', '__class__', '__contains__', '__delattr__', '__delitem__', '__dir__', '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__', '__gt__', '__hash__', '__iadd__', '__imul__', '__init__', '__init_subclass__', '__iter__', '__le__', '__len__', '__lt__', '__mul__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__reversed__', '__rmul__', '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__', 'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']
```

<br>

pass、break、continue
------
`004.pass-break-continue.py`
+ pass : 什麼都不做 `用來維持程式碼的完整性`
+ break : 離開
+ continue : 略過

### pass
```py
print("start => pass")

isFlag = False
if isFlag:
    print("flag = true")
else:
    pass

print("end => pass")
```

Output :
```
start => pass
end => pass
```

    有時在某個區塊中，並不想做任何的事情，或者是稍後才會寫些什麼，
    對於還沒打算寫任何東西的區塊，可以放個pass。

<br>

### break
```py
lstData = ['001', '002', '003', '004', '005']

for text in lstData:
    if text == '003':
        break
    print("text => ", text)
```

Output :
```
text =>  001
text =>  002
```

+ 迴圈中遇到 break，中斷迴圈的迭代動作，可用來中斷 while 迴圈、for in 的迭代

<br>

### continue
```py
lstData = ['001', '002', '003', '004', '005']

for text in lstData:
    if text == '003':
        continue
    print("text => ", text)
```

Output :
```
text =>  001
text =>  002
text =>  004
text =>  005
```

+ 迴圈中遇到 continue，此次迭代不執行後續的程式碼，直接進行下次迭代

<br>


for Comprehension `Python`
------
`005.forComprehension.py`

    
    Python 針對「將一個 1ist 轉為另一個 1ist」的需求，提供了forComprehension語法。
    forComprehension 可以與條件式結合，構成一個過濾的功能。

### 原始 list 資料轉換
`轉換成平方數值`

```py
lstNum = [10, 20, 30]
lstResult = []
for num in lstNum:
    lstResult.append(num ** 2)
print(lstResult)
```

Output :
```
[100, 400, 900]
```

### for Comprehension 語法
```py
lstNum = [10, 20, 30]
lstResult = [num ** 2 for num in lstNum]
print(lstResult)
```

Output :
```
[100, 400, 900]
```

### for Comprehension 加上條件式 `過濾`
```py
lstNum = [10, 20, 30]
lstResult = [num ** 2 for num in lstNum if num > 15]
print(lstResult)
```

Output :
```
[400, 900]
```

### for Comprehension 巢狀迴圈
`遍歷，排列組合`

```py
lstX = ['A', 'B', 'C']
lstY = ['001', '002', '003']
lstText = [x + y for x in lstX for y in lstY]
print(lstText)
```

Output :
```
['A001', 'A002', 'A003', 'B001', 'B002', 'B003', 'C001', 'C002', 'C003']
```

<br>

### Generator 物件

    當資料來源具有惰性求值特性的產生器時，直接產生1ist顯得沒有效率。
    這時可以在 forComprehension兩旁放上()，建立具同樣有惰性求值特性的 generator 物件。

```py
num = sum([n for n in range(1, 100)])
print(num)

num = sum(n for n in range(1, 100))
print(num)
```

Output :
```
4950
4950
```

+ [惰性求值](https://zh.wikipedia.org/wiki/%E6%83%B0%E6%80%A7%E6%B1%82%E5%80%BC)
  + 資料在取用時，才會動態產生
  

**產生集群型態**

```py
lstData = [n for n in range(1, 10)]
print(lstData)

setData = {n for n in range(1, 10)}
print(setData)

lstKey = ['001', '002', '003']
lstVal = ['A', 'B', 'C']
mapData = {key: val for key, val in zip(lstKey, lstVal)}
print(mapData)

data = tuple(n for n in range(1, 10))
print(data)
```

Output :
```
[1, 2, 3, 4, 5, 6, 7, 8, 9]
{1, 2, 3, 4, 5, 6, 7, 8, 9}
{'001': 'A', '002': 'B', '003': 'C'}
(1, 2, 3, 4, 5, 6, 7, 8, 9)
```

+ forComprehension 兩旁放上 [] => 表示會產生 1ist
+ forComprehension 兩旁放上 {} => 表示會產生 set
+ {key : val for key , val in lst} => 產生 dict
  + zip() 函式 : 將兩個序列的各元素，像拉鏈般一對一配對
+ tuple 函式傳入 forComprehension => 產生 tuple

[Python FAQ]:https://docs.python.org/3/faq/design.html#why-isn-t-there-a-switch-or-case-statement-in-python

