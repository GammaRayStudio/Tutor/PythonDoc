Python Flask
======
`Python 輕量級 Web 框架`



pip 指令
------
`安裝 Flask 模組`

+ pypi : <https://pypi.org/project/Flask/>

```bash
pip install flask
```

<br>

Sample Code
------
### WebSE.py
```py
from flask import Flask
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World'

if __name__ == '__main__':
    app.run()
```

**run**
```bash
python WebSE.py
```

**test**
+ <http://127.0.0.1:5000/hello>
+ <http://localhost:5000/hello>


<br>

**Source Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonSE>

```
PythonSE/FlaskSE/WebSE.py
```

<br>

### Render.py
```py
from flask import Flask, render_template
app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World'

@app.route('/html')
def html():
    return '<html><body><h1>Hello World</h1></body></html>'

@app.route('/home')
def home():
    return render_template('home.html')

if __name__ == '__main__':
    app.run()
```

**templates/home.html**
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
</head>
<body>
    <h1>My website</h1>
</body>
</html>
```

**run**
```bash
python Render.py
```

**test**
+ <http://127.0.0.1:5000/hello>
+ <http://localhost:5000/hello>

<br>

**Source Code**
+ <https://gitlab.com/GammaRayStudio/Tutor/PythonSE>

```
PythonSE/FlaskSE/Render.py
PythonSE/FlaskSE/templates/home.html
```



