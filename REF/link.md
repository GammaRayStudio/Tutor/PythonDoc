Python Doc, Reference
======


Contents
------
+ Python 官方資訊
+ Tutorialspoint
+ Django 網頁框架


Python 官方資訊
------
### Python 官網
<https://www.python.org/>

### Python 官方教學
<https://docs.python.org/zh-tw/3/tutorial/index.html>

Tutorialspoint
------
### Python3
<https://www.tutorialspoint.com/python3/index.htm>

### Django
<https://www.tutorialspoint.com/django/index.htm>


Django 網頁框架
------
### Python 入門
<https://djangogirlstaipei.herokuapp.com/tutorials/python/?os=windows>


Python 技術手冊
------
### Python 3.7 技術手冊
<https://github.com/JustinSDK/Python37Tutorial>

### 投影片
+ <https://openhome.cc/Gossip/Books/index.html>
+ <https://openhome.cc/Gossip/Books/Python37ToC.html>


### Google Play
<https://books.google.com.tw/books/about/Python_3_7_%E6%8A%80%E8%A1%93%E6%89%8B%E5%86%8A_%E9%9B%BB%E5%AD%90%E6%9B%B8.html?id=FGZ-DwAAQBAJ&redir_esc=y>

