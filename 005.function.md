函式
======
`Function`

Function
------

    f(x) = 2x + y

    input : x = 2 , y = 1
    output : f(x) = 5

```c
int cal2XPlusY(int a , int b){
  int c = 0;
  c = 2a + b;
  return c;
}
```

**C 語言**

   當敘述的計算重複出現，透過函式來減少重複代碼

<br>


使用 def 定義函式
------
`001.defFunc.py`
```py
def calSum(a, b):
    return a + b
result = calSum(3, 2)
print(" 3 + 2 = ", result)
```

+ def : 定義函式的關鍵字
+ calSum : 函式名稱
+ a , b : 參數名稱
+ return : 回傳值

<br>


**語法**
```py
def 函式名稱 (參數列表) :
    陳述動作
}
```

+ 函式名稱 : Python 不支援函式重載(Overload)實作
  + 同一個名稱空間中，不能有相同的函式名稱
  + 如果定義兩個具有相同名稱的函式，後方定義的會覆蓋前面的
+ 參數列表 : 動態定型語言，不需要知曉型態
+ 陳述動作 : 函式執行完，沒有 return 或指定傳回值，預設會傳回 None

<br>

### 參數預設值
```py
def printMsg(name, version="1.0.1"):
    print("name = ", name, " , version = ", version)
printMsg("PythonSE")
printMsg("PythonFlask", "1.0.2")
```

Output :
```
name =  PythonSE  , version =  1.0.1
name =  PythonFlask  , version =  1.0.2
```

**注意: 物件參數會一直存在**

```py
def genListData(element, lstData=[]):
    lstData.insert(0, element)
    return lstData

lstData = genListData(10)
print(lstData)
lstData = genListData(10, [20, 30])
print(lstData)
lstData = genListData(20)
print(lstData)
```

Output :
```
[10]
[10, 20, 30]
[20, 10]
```

+ 沒有指定 lstData 時，使用的就會是一開始指定的 list 物件

<br>

**調整宣告與寫法**
```py
def genListAdjust(element, lstData=None):
    lstData = lstData if lstData else []
    lstData.insert(0, element)
    return lstData

lstData = genListAdjust(10)
print(lstData)
lstData = genListAdjust(10, [20, 30])
print(lstData)
lstData = genListAdjust(20)
print(lstData)
```

Output :
```
[10]
[10, 20, 30]
[20]
```

<br>

### 關鍵字引數
`呼叫函式時，指定參數名稱來設定引數值`

```py
def queryByParam(index, name, version):
    print(index, ":", "name = ", name, " , version = ", version)


queryByParam(index=1, name="Python", version="1.0.1")
queryByParam(index=2, version="1.0.1", name="Python")
```

Output :
```
1 : name =  Python  , version =  1.0.1
2 : name =  Python  , version =  1.0.1
```

<br>


變量參數
------
`* 與 **`

### List 或 Tuple 參數 拆解與裝配
`*`

**自動拆解物件**

```py
def printMsg(name, version, author):
    print("name = {} , version = {} , author = {}".format(name, version, author))

lstData = ["PythonSE", "1.0.1", "Enoxs"]
print("list => ")
printMsg(*lstData)

print("tuple => ")
lstData = ("PythonSE", "1.0.1", "Enoxs")
printMsg(*lstData)
```

Output :
```
list => 
name = PythonSE , version = 1.0.1 , author = Enoxs
tuple => 
name = PythonSE , version = 1.0.1 , author = Enoxs
```

**自動裝配物件**
```py
def calTotal(*numbers):
    total = 0
    for num in numbers:
        total += num
    return total


result = calTotal(1, 2, 3, 4, 5)
print("result => ", result)
```

Output :
```
result =>  15
```

### Dict 參數 拆解與裝配
`**`

**自動拆解物件**
```py
def printDictMsg(name, version, author):
    print("name = {} , version = {} , author = {}".format(name, version, author))


mapData = {'name': 'PythonSE', 'version': '1.0.2', 'author': 'Enoxs'}
printDictMsg(**mapData)
```

Output :
```
name = PythonSE , version = 1.0.2 , author = Enoxs
```

**自動裝配物件**
```py
def queryByParam(**param):
    msg = "name = {} , version = {} , author = {}".format(
        param['name'],
        param['version'],
        param['author']
    )
    print(msg)


queryByParam(name="PythonSE", version="1.0.3", author="Enoxs")
```

Output :
```
name = PythonSE , version = 1.0.3 , author = Enoxs
```

### 函式接收任何參數引數
```py
def inputAny(*var01, **var02):
    if len(var01) > 0:
        print("list : ", var01)
    if len(var02) > 0:
        print("dict : ", var02)

inputAny(1, 2, 3)
inputAny(a='001', b='002', c='003')
```

Output :
```
list :  (1, 2, 3)
dict :  {'a': '001', 'b': '002', 'c': '003'}
```

<br>

遞迴
------
`003.recursion.py`

### 迴圈
```py
for i in range(10):
    pass
```

+ 重複做十次

### 遞迴
```
f(x) = x / 2
input : x = f(100)
output : f(f(100)) = 25
```

    函式呼叫函式本身

**計算 : 重複減半後，小於 50 的數**
```py
def calHalfLess50(number):
    result = number / 2
    if result > 50:
        result = calHalfLess50(result)

    return result


num = 120
result = calHalfLess50(num)
print("calHalfLess50 => ", result)
```

Output :
```
calHalfLess50 =>  30.0
```

**計算 : 最大公因數**
```py
def calGCD(a, b):
    result = 0
    if a % b == 0:
        result = b
    else:
        result = calGCD(b, a % b)
    return result


num01 = 50
num02 = 100
result = calGCD(num01, num02)
print("calGCD => ", result)
```

Output :
```
calGCD =>  50
```

<br>

區域函式 `Local function`
------
`004.localFunc.py`

    Python 特性，函式中還可以定義函式

```py
def printMsg(name, message):
    def printMsgFormat01():
        print("#01 => ", "name : ", name, ", text : ", message)

    def printMsgFormat02():
        print("#02 => ", "name : ", name, ", text : ", message)

    def printMsgFormat03():
        print("#03 => ", "name : ", name, ", text : ", message)

    if name == "title01":
        printMsgFormat01()
    elif name == "title02":
        printMsgFormat02()
    elif name == "title03":
        printMsgFormat03()


printMsg("title01", "message01")
printMsg("title02", "message02")
printMsg("title03", "message03")
```

Output :
```
#01 =>  name :  title01 , text :  message01
#02 =>  name :  title02 , text :  message02
#03 =>  name :  title03 , text :  message03
```

<br>

一級函式的運用
------
`005.firstClassFunc.py`

    Python 特性，函式不單只是個定義，還是個值。

    當定義函式時，會產生一個函式物件，代表著某個可重用流程的封裝。
    如此，就可以將某個可重用流程進行傳遞，是個極具威力的功能。

    函式跟數值、list、set、dict、tuple 等一樣，都被Python視為一級公民來對待，
    可以自由地在變數、函式呼叫時指定，因此具有這樣特性的函式，也被稱一級函式(First_classfunction)。


<br>

### 傳入物件函式
`filter #1`

```py
def filterData(predicate, lstData):
    result = []
    for data in lstData:
        if predicate(data):
            result.append(data)
    return result

def lenGreaterThan6(lstData):
    return len(lstData) > 6

def lenLessThan5(lstData):
    return len(lstData) < 5

def hasCharI(lstData):
    return 'i' in lstData

lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print('大於 6：', filterData(lenGreaterThan6, lstData))
print('小於 5：', filterData(lenLessThan5, lstData))
print('有個 i：', filterData(hasCharI, lstData))
```

Output :
```
大於 6： ['ABCDEFGHIJK', 'ijkijkijk']
小於 5： ['abc']
有個 i： ['ijkijkijk']
```

<br>

### 回傳物件函式
`filter #2`

```py
def filterFunc(predicate, lstData):
    result = []
    for elem in lstData:
        if predicate(elem):
            result.append(elem)
    return result

def lenGreaterThan(num):
    def lenGreaterThanNum(elem):
        return len(elem) > num
    return lenGreaterThanNum

lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print('大於 5：', filterFunc(lenGreaterThan(5), lstData))
print('大於 7：', filterFunc(lenGreaterThan(7), lstData))
```

Output :
```
大於 5： ['ABCDEFGHIJK', 'ijkijkijk']
大於 7： ['ABCDEFGHIJK', 'ijkijkijk']
```

### 傳入標準庫函式
`map`

```py
def mapData(mapper, lstData):
    result = []
    for ele in lstData:
        result.append(mapper(ele))
    return result


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print(mapData(str.upper, lstData))
print(mapData(len, lstData))
```

Output :
```
['ABC', 'ABCDEFGHIJK', 'IJKIJKIJK']
[3, 11, 9]
```

<br>

### 標準庫函式參數接收
`filter map`

```py
def lenGreaterThan(num):
    def lenGreaterThanNum(elem):
        return len(elem) > num
    return lenGreaterThanNum


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']
print(list(filter(lenGreaterThan(6), lstData)))
print(list(map(len, lstData)))
```

Output :
```
['ABCDEFGHIJK', 'ijkijkijk']
[3, 11, 9]
```

<br>

Lambda 運算式
------
`006.lambda.py`

    當函式本體很簡單，只有一句簡單運算的情況，可以考慮使用 lambda 運算式。

### 替換簡單的運算式
`filter => lambda`
```py
def filterData(predicate, lstData):
    result = []
    for data in lstData:
        if predicate(data):
            result.append(data)
    return result


lstData = ['abc', 'ABCDEFGHIJK', 'ijkijkijk']

print('大於 6：', filterData(lambda elem: len(elem) > 6, lstData))
print('小於 5：', filterData(lambda elem: len(elem) < 5, lstData))
print('有個 i：', filterData(lambda elem: 'i' in elem, lstData))
```

Output :
```
大於 6： ['ABCDEFGHIJK', 'ijkijkijk']
小於 5： ['abc']
有個 i： ['ijkijkijk']
```

### 簡單的函式定義
`lambda => def func()`

```py
max = (lambda n1, n2: n1 if n1 > n2 else n2)
print(max(10, 5))
```

Output :
```
10
```

### Switch 功能，使用 Dict 與 Lambda 實作
`switch => dict / lambda`

```py
score = 86

level = score // 10
{
    10: lambda: print('Perfect'),
    9: lambda: print('A'),
    8: lambda: print('B'),
    7: lambda: print('C'),
    6: lambda: print('D')
}.get(level, lambda: print('E'))()
```

Output :
```
B
```

switch :
```
switch(level){
  case 10 : 
      print("Perfect")
  case 9 :
      print("A")
  case 8 :
      print("B")
  case 7 :
      print("C")
  case 6 :
      print("D")
  default :
      print("E")
}
```

+ dict 的值是 lambda 建立的函式物件，使用 get() 方法取得
+ 若傳入的鍵不存在，get() 傳回第二個引數，模擬 default 部分


書籍提示 :
```
相較於其他語言中的 lambda 語法，Python 使用 lambda 關鍵字的方式，
其實並不簡潔，甚至有些妨礙可讀性。

實際上，Python中的lampbda也沒辦法寫太複雜的邏輯，
這是 Python 中為了避免 lambda 被濫用而特意做的限制，

如果覺得可讀性不佳或者需要撰寫更複雜的邏輯，
請乖乖使用 def 定義函式，並給予一個清楚易懂的函式名稱。
```

變數範圍
------
```py
text = '全域變數'
def funcGobal():
    print("#1 : ", text)

def funcLocal():
    text = "區域變數"
    print("#2 : ", text)

funcGobal()
funcLocal()
print("#3 : ", text)
```

Output :
```
#1 :  全域變數
#2 :  區域變數
#3 :  全域變數
```

+ 全域變數以模組檔案為分界
+ 取用變數時，由內向外尋找

<br>

### locals() 函式 與 globals() 函式
`查詢區域變數的名稱與值`

```py
x = 1
def outerFunc():
    y = 2

    def innerFunc():
        z = 3
        print('inner func locals:', locals())
    innerFunc()
    print('outer func locals:', locals())

outerFunc()
print('local():', locals())
print('global():', globals())
```

Output:
```
local() 與 global() 函式 :
inner func locals: {'z': 3}
outer func locals: {'y': 2, 'innerFunc': <function outerFunc.<locals>.innerFunc at 0x7fcbd12ed5e0>}
local(): {'__name__': '__main__', '__doc__': None, '__package__': None, '__loader__': <_frozen_importlib_external.SourceFileLoader object at 0x7fcbd11703d0>, '__spec__': None, '__annotations__': {}, '__builtins__': <module 'builtins' (built-in)>, '__file__': '007.varScope.py', '__cached__': None, 'text': '全域變數', 'funcGobal': <function funcGobal at 0x7fcbcffba160>, 'funcLocal': <function funcLocal at 0x7fcbd12ed670>, 'x': 1, 'outerFunc': <function outerFunc at 0x7fcbd12ed790>}
global(): {'__name__': '__main__', '__doc__': None, '__package__': None, '__loader__': <_frozen_importlib_external.SourceFileLoader object at 0x7fcbd11703d0>, '__spec__': None, '__annotations__': {}, '__builtins__': <module 'builtins' (built-in)>, '__file__': '007.varScope.py', '__cached__': None, 'text': '全域變數', 'funcGobal': <function funcGobal at 0x7fcbcffba160>, 'funcLocal': <function funcLocal at 0x7fcbd12ed670>, 'x': 1, 'outerFunc': <function outerFunc at 0x7fcbd12ed790>}
```

+ global : 內建模組 (Builtin)


### global 關鍵字
`針對全域範圍的變數`

```py
galX = 100
galY = 200

def func():
    global galX, galY
    galX = 200
    galY = 300

func()
print("x => ", galX)
print("y => ", galY)
```

Output :
```
x =>  200
y =>  300
```

### nonlocal 關鍵字
`指明變數非區域變數`

```py
def funcLocal():
    locX = 100
    locY = 200

    def local():
        nonlocal locX, locY
        locX = 200
        locY = 300

    local()
    print("x => ", locX)
    print("y => ", locY)

funcLocal()
```

Output :
```
x =>  200
y =>  300
```

yield 與 yield from
------
`yield 運算式產值，相似於 return ，是將流程控制權讓給函式的呼叫者`

```py
def xrange(n):
    x = 0
    while x != n:
        yield x
        x += 1

for n in xrange(10):
    print(n)
```

Output :
```
0
1
2
3
4
5
6
7
8
9
```

### generator 物件
`yield 指定一個變數時，傳回的是一個 generator 物件`

```py
g = xrange(10)
print("type => ", type(g))
print("next(0) => ", next(g))
print("next(1) => ", next(g))
```

Output :
```
type =>  <class 'generator'>
next(0) =>  0
next(1) =>  1
```

### yield 運算式
`可以透過 send() 方法指定值，實作生產者與消費者程式`

```py
count = 0

def producer():
    while True:
        global count
        count += 1
        print('producer ：', count)
        yield count

def consumer():
    while True:
        count = yield
        print('consumer ：', count)

def clerk(jobs, producer, consumer):
    print('run {} time'.format(jobs))
    p = producer()
    c = consumer()
    next(c)
    for i in range(jobs):
        count = next(p)
        c.send(count)

clerk(3, producer, consumer)
```

Output :
```
run 3 time
producer ： 1
consumer ： 1
producer ： 2
consumer ： 2
producer ： 3
consumer ： 3
```















