模組管理
======


複習
------
```
一個 .py 檔案就是一個橫組，可以使用 dir() 函式，知道一個橫組中有哪些名稱。
```

<br>

用途
------
`抽象層的封裝與隔離`

```
只要知道抽象層的介面外觀，不用知道具體實作細節。
```

<br>

流程
------
### import
```
當 import 某個橫組而使得指定的 .py 檔案被載人時，
python 直譯器會為它建立一個 module 實例，並建立一個模組名稱來參考它。
```

### form import
```
from import 會將被匯人橫組中之名稱參考的值，指定給目前橫組中建立的新名稱。
(也就是在模組中建立新變數，而不是用原本模組的變數。)
```

### 程式碼
`注意 : 參考可變動物件部分`

**spport.py**
```py
x = 100
y = 200
text = ['001', '002', '003']
```

**001.moduleRun.py**
```py
from support import x
from support import y
from support import text
import support as sup
import support

x = 500
y = 500

# create new variable
print("support.x => ", support.x)
print("support.y => ", support.y)
print("sup.x => ", sup.x)
print("sup.y => ", sup.y)

# modify object
print("text => ", text)

text[0] = "AAA"
print("support.text => ", support.text)
print("sup.text => ", sup.text)
```

Output :
```
support.x =>  100
support.y =>  200
sup.x =>  100
sup.y =>  200
text =>  ['001', '002', '003']
support.text =>  ['AAA', '002', '003']
sup.text =>  ['AAA', '002', '003']
```

<br>

限制 from impor *
------
### 限制建立同名變數
`用底線作為開頭`

**support.py**
```py
x = 100
y = 200
text = ['001', '002', '003']

# limit import var
_limit = "LimitText"
```

**002.varLimit.py**
```py
from support import *
import support as sup
print("x => ", x)
print("y => ", y)
print("text => ", text)
print("sup._limit => ", sup._limit)
```

Output :
```
x =>  100
y =>  200
text =>  ['001', '002', '003']
sup._limit =>  LimitText
```

+ 使用 `_limit` 變數會出現 NameError 錯誤，描述變數未定義
+ import 模組同樣可以使用底線開頭的變數

<br>

### 定義 `__all__` 變數
`只允許名單中的變數導入`

**module.py**
```py
__all__ = ['x', 'text']
x = 100
y = 200
text = ['001', '002', '003']
_limit = "LimitText"
```

**003.varAll.py**
```py
from module import *
import module as mod

print("x => ", x)
print("text => ", text)

print("mod.y => ", mod.y)
print("mod._limit => ", mod._limit)
```

Output :
```
x =>  100
text =>  ['001', '002', '003']
mod.y =>  200
mod._limit =>  LimitText
```

<br>

del() 方法
-----
`刪除 import 模組或 form import 名稱`

**004.delModule.py**
```py
import support
from support import x

isExist = 'support' in dir()
print("Support => ", isExist)

del support

isExist = 'support' in dir()
print("Support => ", isExist)


isExist = 'x' in dir()
print("x => ", isExist)

del x

isExist = 'x' in dir()
print("x => ", isExist)

# import support as sup
if 'support' not in dir():
    import support
    sup = support
    print('sup.x => ', sup.x)

```

Output :
```
Support =>  True
Support =>  False
x =>  True
x =>  False
sup.x =>  100
```

sys.modules
------
`sys.modules 可以知道目前已載入的 module 名稱與實例`

**005.sysModule.py**
```py
import sys
import support
import module

isSupportExist = 'support' in sys.modules
isModuleExist = 'module' in sys.modules

print("isSupportExist => ", isSupportExist)
print("isModuleExist => ", isModuleExist)
```

Output :
```
isSupportExist =>  True
isModuleExist =>  True
```

<br>

模組名稱範圍
------

**006.importAnyWhere.py**
```py
def supportImport():
    import support
    print("x => ", support.x)
    print("y => ", support.y)
    print("local() => ", locals())

def isSupportExist():
    import sys
    return 'support' in sys.modules

supportImport()
print("global() => ", globals())
```

Output :
```
x =>  100
y =>  200
local() =>  {'support': <module 'support' from '/Users/Enoxs/Mac_Document/WorkSpace/GammaRayStudio/Tutor/PythonSE/Module/support.py'>}
global() =>  {'__name__': '__main__', '__doc__': None, '__package__': None, '__loader__': <_frozen_importlib_external.SourceFileLoader object at 0x7fbbe515b400>, '__spec__': None, '__annotations__': {}, '__builtins__': <module 'builtins' (built-in)>, '__file__': '006.importAnyWhere.py', '__cached__': None, 'supportImport': <function supportImport at 0x7fbbe36ba160>, 'isSupportExist': <function isSupportExist at 0x7fbbe52ce670>}
```

+ import、import as、from import 可以出現在陳述句能出現的位置，
  + if..else 區塊
  + def func() 函式
  + `根據不同的情況進行不同的 import`
+ 一般的 import 為全域範圍，函式內的 import 就為區域範圍，跟變數的宣告一樣

<br>

設定 PTH 檔案
------
### sys.path 與 site.getsitepackage()

**007.sysPath.py**
```py
import sys
import site

print(sys.path)
print(site.getsitepackages())
```

Output :
```
['/Users/Enoxs/Mac_Document/WorkSpace/GammaRayStudio/Tutor/PythonSE/Module', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python38.zip', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/lib-dynload', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/site-packages']
['/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/site-packages']
```

sys.path 清單列出尋找模組時的路徑 : 
清單內容基本上可來自於幾個來源:
+ 執行 python 直譯器時的資料夾
+ PYTHONPATH 環境變數
+ Python 安裝中標準程式庫等資料夾
+ PTH 檔案列出的資料夾 `site.getsitepackags()`
  + 不同作業系統並不相同
  + site 模組的 getsitepackages() 函式可以取得位置

### site.addsitedir()
`使用 site.addsitedir() 函式，新增 PTH 檔案的資料夾來源`


**ModulePath/Message.py**
```py
msg = "Module Message"
```

**007.sysPath.py**
```py
site.addsitedir("ModulePath")
print(sys.path)

def printModuleMsg():
    import Message
    print("message => ", Message.msg)


printModuleMsg()
print(sys.path)

def printModuleMsg():
    import Message
    print("message => ", Message.msg)

printModuleMsg()
```

Output :
```
['/Users/Enoxs/Mac_Document/WorkSpace/GammaRayStudio/Tutor/PythonSE/Module', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python38.zip', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/lib-dynload', '/Library/Frameworks/Python.framework/Versions/3.8/lib/python3.8/site-packages', '/Users/Enoxs/Mac_Document/WorkSpace/GammaRayStudio/Tutor/PythonSE/Module/ModulePath']
message =>  Module Message
```




