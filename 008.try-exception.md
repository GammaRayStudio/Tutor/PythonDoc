Python 例外處理
======


Contents
------
+ 使用 try、except 處理例外
+ 認識例外繼承架構
+ 認識 raise 使用時機
+ 運用 finally 清除資源
+ 使用 with as 管理資源


try、except 處理例外
------
### 範例 : 輸入數字，計算平均
`001.try-except.py`

```py
lstNum = input("Please input number:").split(" ")
average = sum(int(num) for num in lstNum) / len(lstNum)
print("average => ", average)
print("\n- - - Done - - -")
```

Input :
```
10 20 30
```

Output :
```
average =>  20.0

- - - Done - - -
```

Input :
```
10 20 A
```

Output :
```
Traceback (most recent call last):
  File "001.try-except.py", line 2, in <module>
    average = sum(int(num) for num in lstNum) / len(lstNum)
  File "001.try-except.py", line 2, in <genexpr>
    average = sum(int(num) for num in lstNum) / len(lstNum)
ValueError: invalid literal for int() with base 10: 'A'
```

---

### 處理例外 : try、except
```py
try:
    lstNum = input("Please input number:").split(" ")
    average = sum(int(num) for num in lstNum) / len(lstNum)
    print("average => ", average)
except ValueError as err:
    print(err)

print("\n- - - Done - - -")
```

Input :
```
10 20 30
```

Output :
```
average =>  20.0

- - - Done - - -
```

Input :
```
10 20 A
```

Output :
```
invalid literal for int() with base 10: 'A'

- - - Done - - -
```

+ try : Python 嘗試執行 try 區塊中的程式碼，如果發生例外，流程會跳離發生點
+ except : 跳離後，比對 except 的型態，若符合引發的例外型態，就執行 except 區塊中的程式碼。

<br>

### 在 Python 中，例外不一定是錯誤 `(Error)`

**迴圈 : for in 語法**

    底層運用到例外處理機制

回顧 :
```
具有 __iter__() 方法的物件，都可以使用 for in 來迭代。
```

實作 :
```
迭代器具有 __next__()方法，每次迭代時就會傳回下一個物件，
而沒有下一個元素時，則會引發 StopIteration 例外
```

**002.try-except-tuple.py**
```py
try:
    lstNum = input("Please input number:").split(" ")
    average = sum(int(num) for num in lstNum) / len(lstNum)
    print("average => ", average)
except ValueError as err:
    print("\ninput number error")
except (EOFError, KeyboardInterrupt):
    print("\nUser Interrupt")
except:
    print("\nOther Exception")
```

Input :
```
10 20 ^C
```

Output :
```
User Interrupt
```

可以使用多個 except :
+ `except (EOFError, KeyboardInterrupt):` -  使用 tuple 指定多個物件
+ `except:` - 若沒有指定 except 物件型態，表示捕捉所有引發的物件


<br>


### Standard Exceptions
| 編號 | 例外名稱 | 描述 |
| --- | --- | --- |
| 1 |	Exception | 所有異常的基類 | 
| 2 |	StopIteration | 當迭代器的 next() 方法不指向任何對象時引發 |
| 3 |	SystemExit | 由 sys.exit() 函數引發 |
| 4 |	StandardError | 除 StopIteration 和 SystemExit 之外的所有內置異常的基類 |
| 5 |	ArithmeticError | 數值計算發生的所有錯誤的基類 |
| 6 |	OverflowError | 當計算超過數字類型的最大限制時引發 |
| 7 |	FloatingPointError | 當浮點計算失敗時引發 |
| 8 |	ZeroDivisonError | 當對所有數字類型進行除法或模數除以零時引發 |
| 9 |	AssertionError | 在 Assert 語句失敗的情況下引發 |
| 10 | AttributeError | 在屬性引用或分配失敗的情況下引發 | 
| 11 | EOFError | 當沒有來自 raw_input() 或 input() 函數的輸入並且到達文件末尾時引發 |
| 12 | ImportError | 當導入語句失敗時引發 |
| 13 | KeyboardInterrupt | 當用戶中斷程序執行時引發，通常是按 Ctrl+c | 
| 14 | LookupError | 所有查找錯誤的基類 | 
| 15 | IndexError | 在序列中未找到索引時引發 |
| 16 | KeyError | 在字典中找不到指定的鍵時引發 | 
| 17 | NameError | 在本地或全局命名空間中找不到標識符時引發 |
| 18 | UnboundLocalError | 在嘗試訪問函數或方法中的局部變量但未為其分配值時引發 |
| 19 | EnvironmentError | Python 環境之外發生的所有異常的基類 |
| 20 | IOError | 當輸入/輸出操作失敗時引發，例如嘗試打開不存在的文件時的打印語句或 open() 函數 |
| 21 | OSError | 引發與操作系統相關的錯誤 | 
| 22 | SyntaxError | 當 Python 語法出現錯誤時引發 |
| 23 | IndentationError | 當未正確指定縮進時引發 | 
| 24 | SystemError | 當未正確指定縮進時引發 |
| 25 | SystemExit | 在使用 sys.exit() 函數退出 Python 解釋器時引發。如果未在代碼中處理，則導致解釋器退出 |
| 26 | TypeError | 在嘗試對指定數據類型無效的操作或函數時引發 |
| 27 | ValueError | 當數據類型的內置函數具有有效的參數類型，但參數指定了無效值時引發 |
| 28 | RuntimeError | 當生成的錯誤不屬於任何類別時引發 | 
| 29 | NotImplementedError | 當需要在繼承的類中實現的抽象方法實際上沒有實現時引發 |

<br>

### 例外繼承架構
```py
print("a / b = ?")
try:
    a = int(input("a = "))
    b = int(input("b = "))
    result = "{} / {} = {}".format(a, b, a/b)
    print("\nresult :")
    print(result)
except ArithmeticError:
    print("ArithmeticError")  # 運算錯誤
except ZeroDivisionError:
    print("ZeroDivisionError")  # 除零錯誤
except:
    print("Exception")
```

Input :
```
a / b = ?
a = 10
b = 5
```

Output :
```
result :
10 / 5 = 2.0
```

+ 在 Python 中，所有的例外都是 BaseException 的子類別，
  + `沒有指定例外型態就是比對此類`
+ 比對例外的過程中，如果符合父類別的型態，就不會去比對子型態
  + `注意先後順去處理，否則例外處理等於沒有定義`

**官方文件**
```
- BaseException
  - GeneratorExit
  - KeyboardInterrupt
  - SystemExit
  - Exception
    - ArithmeticError
      - FloatingPointError
      - OverflowError
      - ZeroDivisionError
    - AssertionError
    - AttributeError
    - BufferError
    - EOFError
    - ImportError
      - ModuleNotFoundError
    - ... 
```

+ Exception 為 Python 標準程式庫的內容，可以在官方文件中，找到完整的例外繼承架構
  + `python3 -m pydoc -p 8080`
+ 要自訂例外，不要直接繼承 BaseException
  + 應該繼承 Exception
  + 或者是 Exception的 相關子類別來繼承
    + `SystemExit` : sys.exit() 引發
    + `KeyboardInterrupt` : 鍵盤中斷
    + `GeneratorExit` : 產生器呼叫 close() 方法引發時

繼承 Exception 的建議 : 
```
自定義例外時，要繼承 Exception !

如果子類別定義了 __init__()，建議將傳入的引數，
透過 super().__init__(arg1,arg2, ...)，來呼叫 父類別 Exception 的 __init__()。

因為 Exception 的 __init__()  預設接受所有傳人的引數，
而這些被接受的全部引數，可透過 args 屬性以一個 tuple 取得。
```

<br>

### 主動引發例外 `raise`

    在 Python 中，實作服務端的工具套件，
    因為某些原因，希望通知客戶端，告知流程必須中斷，無法繼續。
    這時候就可以使用 raise 主動引發例外

**004.raise-except.py**
```
class CalUtil:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def checkNumA(self, a):
        if a > 0:
            self.a = a
        else:
            raise ValueError("數字 A 必須大於 0")

    def checkNumAandB(self, a, b):
        if a > b:
            self.a = a
            self.b = b
        else:
            raise CalculateException("數字 B 必須小於 A")

    def calNum(self):
        return self.a / self.b

class CalculateException(Exception):
    def __init__(self, message):
        super().__init__(message)

cal = CalUtil(10, 5)
print(cal.calNum())

try:
    cal.checkNumA(-5)
except ValueError as err:
    print(err)

try:
    cal.checkNumAandB(5, 10)
except CalculateException as err:
    print(err)
```

Output:
```
2.0
數字 A 必須大於 0
數字 B 必須小於 A
```

<br>

**使用 raise 將 except 比對的例外，重新引發**

```py
try:
    try:
        cal.checkNumA(-10)
    except ValueError as err:
        raise
except Exception as err:
    print("ValueException => ", err)
```

Output :
```
ValueException =>  數字 A 必須大於 0
```

<br>

**使用 raise from 將比對的例外，使用自訂的例外類型，重新引發**

```py
try:
    try:
        cal.checkNumA(-10)
    except ValueError as err:
        raise CalculateException("數字 A 必須大於 0") from err
except Exception as err:
    print("CalculateException => ", err)
```

Output :
```
CalculateException =>  數字 A 必須大於 0
```

<br>


Python 例外風格
------

    在 Python 中，例外並不一定是錯誤，列如 SystemExit、GeneratorExit、KeyboardInterrupt，
    或者是 StopIteration 等，更像是一種事件，代表著流程因為某個原因無法繼續而必須中斷。

<br>

### 主動引發例外 `raise`

    主動引發一個例外並不是嫌程式中的臭蟲不夠多，而是對呼叫者善盡告知的責任。
    在 Python 中，就算例外是個錯誤，只要程式碼能明確表達出意圖的情況下，也常會當成是流程的一部份。

<br>

### importError
`使用 try except 處理 import 的模組`

**message.py**
```py
msg = ""
def printMsg():
    print(msg)
```

**005.import-module.py**
```py
try:
    import msg
except ImportError:
    import message

message.msg = "Hello"
message.printMsg()
```

Output :
```
Hello
```

<br>

例外的堆疊追蹤
------
`用 traceback 模組，可以知曉多重呼叫下例外的傳播過程，也就是發生例外的根源`

```py
import traceback
def funcA():
    text = None
    return text.upper()

def funcB():
    funcA()

def funcC():
    funcB()

try:
    funcC()
except:
    traceback.print_exc()
    traceback.print_exc(file=open('traceback.txt', 'w+'))
```

Output :

```
Traceback (most recent call last):
  File "006.traceback.py", line 18, in <module>
    funcC()
  File "006.traceback.py", line 14, in funcC
    funcB()
  File "006.traceback.py", line 10, in funcB
    funcA()
  File "006.traceback.py", line 6, in funcA
    return text.upper()
AttributeError: 'NoneType' object has no attribute 'upper'
```

+ `print_exc()` : 查看堆疊追蹤最簡單的方法
  + `print_exception(*sys.exc_info(),limit,file,chain)` 的便捷(shorthand)方法
+ `traceback.print_exc(file=open('traceback.txt', 'w+'))` : 將追蹤訊息寫入到 traceback.txt 檔案

### sys 模組的 exc_info() 方法

```py
import sys

def funcA():
    raise Exception("Something Happened.")

try:
    funcA()
except:
    print(sys.exc_info(), "\n")

    exType, exObj, exTrace = sys.exc_info()
    print("Type : ", exType)
    print("Object : ", exObj)

    print("\nTrace : \n")
    while exTrace:
        code = exTrace.tb_frame.f_code
        print("File Name : ", code.co_filename)
        print("Func or File Name : ", code.co_name)
        exTrace = exTrace.tb_next
        print("\n- - - - - - - -\n")

```

Output :

```
(<class 'Exception'>, Exception('Something Happened.'), <traceback object at 0x7f9b16ac0340>) 

Type :  <class 'Exception'>
Object :  Something Happened.

Trace : 

File Name :  007.sys_exc_info.py
Func or File Name :  <module>

- - - - - - - -

File Name :  007.sys_exc_info.py
Func or File Name :  funcA

- - - - - - - -
```
+ `exType, exObj, exTrace = sys.exc_info()`: 取得 tuple 物件
  + `exType` : 例外的類型
  + `exObj` : 例外的實例
  + `exTrace` : traceback 物件
+ `tb_frame` : 該層追蹤的所有物件資訊
  + `f_code` : 取得該層的程式碼資訊
    + `co_filename` : 該程式碼所在的檔案
    + `co_name` : 取得函式或模組名稱

### sys 模組的 excepthook() 方法
`不使用 try、except ，同樣比對程式中的例外`

```py
import sys

def handleExcept(exType, exObj, exTrace):
    print("Type : ", exType)
    print("Object : ", exObj)

    print("\nTrace : \n")
    while exTrace:
        code = exTrace.tb_frame.f_code

        print("File Name : ", code.co_filename)
        print("Func or File Name : ", code.co_name)

        exTrace = exTrace.tb_next
        print("\n- - - - - - - -\n")

sys.excepthook = handleExcept

def funcA():
    raise Exception("Something Happened.")

funcA()

```

Output :

```
Type :  <class 'Exception'>
Object :  Something Happened.

Trace : 

File Name :  008.sys_excepthook.py
Func or File Name :  <module>

- - - - - - - -

File Name :  008.sys_excepthook.py
Func or File Name :  funcA

- - - - - - - -
```

+ 註冊 sys.excepthook 方式，得到 except 拋出的例外結果 
+ 交由 handleExcept 處理例外資訊


例外的資源管理
------

    當發生例外時，原本的程式流程就會被中斷，
    如果原先已經開啟了資源，就要使用以下的方法來正常的關閉。


### try、except、else
```py
nums = input("please input number :").split(' ')
try:
    lstNum = [int(num) for num in nums]
except ValueError as err:
    print(err)
else:
    print("average => ", sum(lstNum) / len(lstNum))
```

Input :
```
10 20 30
```

Output :
```
average =>  20.0
```

Input :
```
10 20 A
```

Output :
```
invalid literal for int() with base 10: 'A'
```

+ try、except 可以搭配 else
+ 使得 try 中的程式碼，看起與引發例外的來源相關。

<br>

### try、except、finally
```py
filename = input("input filename: ")

try:
    print("filename :", filename)
    f = open(filename, 'r')
except FileNotFoundError as err:
    print("Can't find file")
else:
    try:
        line = len(f.readlines())
        print("total lines :", line)
    finally:
        f.close()
        print("finally")
```

Input :
```
traceback.txt
```

Output :
```
filename : traceback.txt
total lines : 10
finally
```

Input :
```
trace.txt
```

Output :
```
filename : trace.txt
Can't find file
```

**finally 之前有 return ?**

```py
def funcA(flag):
    try:
        if flag:
            return 1
    finally:
        print("finally")
    return 0

funcA(True)
```

Output :

```
finally
1
```

+ finally 區塊先執行完後，才回傳值

<br>

### with as
```py
filename = input("input filename: ")

try:
    with open(filename, 'r') as f:
        print("filename : ", filename)
        print("lines : ", len(f.readlines()))
except FileNotFoundError:
    print("Can't find file")
```

Input :
```
traceback.txt
```

Output :

```
filename :  traceback.txt
lines :  10
```

Input :
```
trace.txt
```

Output :
```
Can't find file
```

+ 使用 with as 方法，自動執行清除資源的動作
  + with 後方銜接資源實例
  + as 方法，指定一個變數
+ with as 不限於檔案使用，只要該實例支援情境管理的協定，就可以使用 with as 語句。


情境管理器
------
`with as 方法，可以支援的實例物件協定`

### 實作 `__enter__()` 與 `__exit__()` 方法

```py
filename = input("input filename: ")
class FileReader:
    def __init__(self, filename):
        self.filename = filename

    def __enter__(self):
        self.file = open(self.filename, 'r')
        return self.file

    def __exit__(self, type, msg, traceback):
        self.file.close()
        return False

try:
    with FileReader(filename) as f:
        print("filename : ", filename)
        print("lines : ", len(f.readlines()))
except FileNotFoundError:
    print("Can't find file")
```

Output :
```
filename :  traceback.txt
lines :  10
```

### 使用 Contextlib 模組的 `@contextmanager` 修飾器
```py
from contextlib import contextmanager

@contextmanager
def file_reader(filename):
    try:
        f = open(filename, 'r')
        yield f  # yield 的物件將作為 as 的值
    finally:
        f.close


with file_reader(filename) as f:
    print("filename : ", filename)
    print("lines : ", len(f.readlines()))
```

Output :

```
filename :  traceback.txt
lines :  10
```

+ `@contextmanager` : 標註函式的內容，會回傳一個實作了情境管理器協定的物件
+ `yeild` : 將資源銜接在 yield 之後，就可以指定為 as 後方的變數值






